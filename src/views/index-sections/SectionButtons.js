import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";


import {
  Container,
  Row,
  Col
} from "reactstrap";
function SectionButtons() {
  const [data, setData] = useState({ hits: [] });



  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://localhost:5000/usersdata',
      );
      setData(result.data);
     
    };

    fetchData();


  }, []);
  return (
    <>
      <div className="section section-buttons">
        <Container>
          <div className="title">
            <h2>Our Astrologer</h2>
          
          </div>
        
          <br />
          <Row>
            {data.hits.map(item => (
              <Col className="profilecard" lg="3" sm="6" key={item.objectID} >
                
      <Link   to={{
    pathname: `/profile-page`,
    state: { id: item.objectID }
  }}>


                <div className="profile-card">
                  <img alt="..." className="img-circle img-no-padding img-responsive" src="/static/media/kaci-baum-2.e06d84cb.jpg" />
                  <p className="text-center">{item.name}</p>
                  <div className="as_profile_text">
                    <ul className="as_profile_text_ul">
                      <li className="wrapText" title="Vedic Astrology, Prashna Kundali"><span className=" astrologer_sprite w25 astro_specialty"></span> {item.experties}</li>
                      <li className="wrapText" title="English, Hindi, Punjabi"> <span className=" astrologer_sprite w25 astro_language"></span> {item.languages}</li>
                      <li> <span className=" astrologer_sprite w25 astro_experience"></span> {item.experience}</li>
                    </ul>
                    <div id="CallStatusAstro">
                      <a  className="online_Callme">CALL : {item.phone}</a>
                    </div>
                  </div>
                </div>
                </Link>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </>
  );
}
export default SectionButtons;