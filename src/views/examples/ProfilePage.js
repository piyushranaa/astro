/*!

=========================================================
* Paper Kit React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// reactstrap components
import {
  Button,
  Label,
  FormGroup,
  Input,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col
} from "reactstrap";


// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import ProfilePageHeader from "components/Headers/ProfilePageHeader.js";
import DemoFooter from "components/Footers/DemoFooter.js";

function ProfilePage(dataa) {


  const [activeTab, setActiveTab] = React.useState("1");
  const [data, setData] = useState({"user":{}});

  const toggle = tab => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  document.documentElement.classList.remove("nav-open");

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://localhost:5000/id?id=' + dataa.location.state.id,
      );
      console.log("=====>",result.data)
      setData(result.data);
      
    };

    fetchData();



  }, []);



  return (
    <>

      <ExamplesNavbar />
      <ProfilePageHeader />

      <div className="section profile-content">
     

        {/* {data.user.map(item => (
          <Col className="profilecard" lg="3" sm="6" key={item.objectID} >




            <div className="profile-card">
              <img alt="..." className="img-circle img-no-padding img-responsive" src="/static/media/kaci-baum-2.e06d84cb.jpg" />
              <p className="text-center">{item.name}</p>
              <div className="as_profile_text">
                <ul className="as_profile_text_ul">
                  <li className="wrapText" title="Vedic Astrology, Prashna Kundali"><span className=" astrologer_sprite w25 astro_specialty"></span> {item.experties}</li>
                  <li className="wrapText" title="English, Hindi, Punjabi"> <span className=" astrologer_sprite w25 astro_language"></span> {item.languages}</li>
                  <li> <span className=" astrologer_sprite w25 astro_experience"></span> {item.experience}</li>
                </ul>
                <div id="CallStatusAstro">
                  <a className="online_Callme">CALL : {item.phone}</a>
                </div>
              </div>
            </div>

          </Col>
        ))} */}


        <Container>
          <div className="owner">
            <div className="avatar">
              <img
                alt="..."
                className="img-circle img-no-padding img-responsive"
                src={require("assets/img/faces/joe-gardner-2.jpg")}
              />
            </div>
            <div className="name">
              <h4 className="title">
              {data && data.user  && data.user.name } <br />
              </h4>
      <h6 className="description">Experience: {data && data.user  && data.user.experience}</h6>
      <h6 className="description">languages: {data && data.user  && data.user.languages}</h6>
            </div>
          </div>
          <Row>
            <Col className="ml-auto mr-auto text-center" md="6">
              <p>
              {data && data.user  && data.user.bio }
              </p>
              <br />
              {/* <Button className="btn-round" color="default" outline>
                <i className="fa fa-cog" /> Settings
              </Button> */}
            </Col>
          </Row>
          <br />
    

     
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

export default ProfilePage;
